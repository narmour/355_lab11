var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');



// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            //console.log(result);
            // get first and last names for resumes put into result
            account_dal.getAll(function(err,account_data){
                if(err){
                    console.log("shit is fucked yo");
                }
                else{
                    //console.log(account_data);
                    for(var i =0;i<result.length;i++){
                        var aid = result[i].account_id;
                        console.log("aid: " + aid);
                        for(var j = 0;j<account_data.length;j++){
                            if(account_data[j].account_id == aid){
                                //console.log("found match");
                                result[i].first_name = account_data[j].first_name;
                                result[i].last_name = account_data[j].last_name;
                            }
                        }
                    }
                    //console.log(result);
                    //result.account_data = account_data;
                    //console.log(result[0]);

                    result.sort(function(a,b){
                        //console.log(a.first_name)
                        if( a.first_name < b.first_name)
                            return -1;
                        else
                            return 1;

                    });
                    //console.log(result);
                    res.render('resume/resumeViewAll', { 'result':result });

                }
            })
            //  console.log(result);
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {

        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new resume form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    /*
    resume_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {'resume': result});
        }
    });
    */
    if(req.query.account_id ==null){
        console.log("need to select user");
    }
    else{
        console.log("req query",req.query);
        resume_dal.edit(req.query.account_id, function(err, result){
            console.log("RESULT FROM ACCOUNT_GETINFO");
            //split up data in a shitty way :)
            var skills = [];
            var companies = [];
            var schools = [];
            console.log(result);
            for(var i =0;i < result.length;i++){
                if(result[i][0]!=null){
                    if('skill_id' in result[i][0]){
                        //console.log("found skill");
                        //console.log(result[i][0]);
                        skills.push(result[i][0]);
                    }
                    else if('company_id' in result[i][0]){
                        companies.push(result[i][0]);
                    }
                    else if('school_id' in result[i][0]){
                        schools.push(result[i][0]);
                    }
                }
            }
            //console.log(schools[0].school_name);
            //console.log("SCHOOLS: ",schools);
            //console.log("COMPANIES: ", companies);
            console.log("SKILLS");
            console.log(skills);
            //console.log(result);
            res.render('resume/resumeAdd', {schools: schools, skills: skills,companies:companies});
        });


        //res.render('resume/resumeAdd',{'result':req.query});

    }

});

// Return the add a new resume form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            //console.log(result);
            res.render('resume/resumeAddSelectUser', {'result': result});
        }
    });
});

// View the resume for the given id
router.post('/insert', function(req, res){
    //console.log("RESUME_ID: ",resume_id);
    //console.log("FROM /insert RESUME ROUTER");
    //console.log(req.body);
    // simple validation
    if(req.body.resume_name == null) {
        res.send('need resume_name');
    }
    else if(req.body.skill_id == null) {
        res.send('need skill');
    }
    else if(req.body.school_id == null) {
        res.send('need school');
    }
    else if(req.body.company_id == null) {
        res.send('need company');
    }

    else {
        //console.log(req.query);
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                //console.log(err)
                res.send(err);
            }
            else {
                console.log("BODY");
                console.log(req.body);
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, 'success');
                //console.log("yo got here");
                //res.send('success yo');
                //res.render('resume/resumeUpdate',{result:req.body})
                resume_dal.edit2(resume_id,function(err,result){
                    console.log("FROM EDIT");
                    console.log(result[0][0].resume_id);

                    res.render('resume/resumeUpdate',{result:result[0],was_successful:true});
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    console.log(req.query);
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit2(req.query.resume_id, function(err, result){
            console.log("FROM RESUME EDIT");
            console.log(result);
            res.render('resume/resumeUpdate', {result: result[0]} );
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err, resume){
            resume_dal.getAll(function(err, resume) {
                res.render('resume/resumeUpdate', {resume: resume[0], resume: resume});
            });
        });
    }

});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

module.exports = router;
