var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM resume;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(resume_id, callback) {
    var query = 'SELECT resume_name,description from' +
        ' resume where resume_id = ?'
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume (account_id,resume_name) VALUES (?,?)';
    var queryData = [params.account_id,params.resume_name];
    connection.query(query, queryData, function(err, result) {
         // THEN USE THE resume_ID RETURNED AS insertId AND THE SELECTED resume_IDs INTO resume_resume
         var resume_id = result.insertId;
         // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
         var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?,?)';
         var queryData = [resume_id,params.school_id[0]];
         connection.query(query,queryData,function(err, result) {
         });

        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?,?)';
        var queryData = [resume_id,params.company_id[0]];
        connection.query(query,queryData,function(err, result) {
            //callback(err, result);
        });

        var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES (?,?)';
        var queryData = [resume_id,params.skill_id[0]];
        connection.query(query,queryData,function(err, result) {
            //callback(err, result);
        });

        callback(err, resume_id);


    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    console.log("RESUME_ID",params.resume_id);
    var queryData = [params.resume_name,params.resume_id];
    console.log("PARAMS: ",params);
    console.log( queryData);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);

    });
};

exports.edit = function(account_id, callback) {
    console.log("calling account_getinfo");
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit2 = function(resume_id, callback) {
    console.log("calling resume_getinfo");
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};