var mysql   = require('mysql');
var db  = require('./db_connection.js');
/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select s.*, a.street, a.zip_code from account s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT a.first_name,a.last_name,a.email from' +
    ' account a where a.account_id = ?'
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email,first_name,last_name) VALUES (?,?,?)';
    console.log(params.account_email,params.account_first,params.account_last)
    var queryData = [params.account_email,params.account_first,params.account_last];
    console.log(queryData);
    connection.query(query, queryData, function(err, result) {
        /*
        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO account_ADDRESS
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_address (account_id, address_id) VALUES (?)';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountAddressData = [];
        for(var i=0; i < params.address_id.length; i++) {
            accountAddressData.push([account_id, params.address_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND accountAddressData
        connection.query(query, [accountAddressData], function(err, result){
            callback(err, result);

        });
        */
        callback(err,result);
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountAddressInsert = function(account_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_address (account_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        accountAddressData.push([account_id, addressIdArray[i]]);
    }
    connection.query(query, [accountAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressInsert = accountAddressInsert;

//declare the function so it can be used locally
var accountAddressDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_address WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressDeleteAll = accountAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET first_name = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.account_id];
    console.log(params);
    console.log("account params: "+  params.account_id);

    connection.query(query, queryData, function(err, result) {
        //delete account_address entries for this account
        //accountAddressDeleteAll(params.account_id, function(err, result){
        /*
            if(params.address_id != null) {
                //insert account_address ids
                accountAddressInsert(params.account_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }o*/
        callback(err, result);

    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo (account_id int)
 BEGIN

 SELECT * FROM account WHERE account_id = _account_id;

 SELECT a.*, s.account_id FROM address a
 LEFT JOIN account_address s on s.address_id = a.address_id AND account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account2_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};