var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(address_id, callback) {
    var query = 'SELECT street,zip_code from' +
        ' address a where a.address_id = ?'
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE address
    var query = 'INSERT INTO address (street,zip_code) VALUES (?,?)';
    var queryData = [params.street,params.zip_code];
    console.log(queryData);
    connection.query(query, queryData, function(err, result) {
        /*
         // THEN USE THE address_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO address_ADDRESS
         var address_id = result.insertId;

         // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
         var query = 'INSERT INTO address_address (address_id, address_id) VALUES (?)';

         // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
         var addressAddressData = [];
         for(var i=0; i < params.address_id.length; i++) {
         addressAddressData.push([address_id, params.address_id[i]]);
         }

         // NOTE THE EXTRA [] AROUND addressAddressData
         connection.query(query, [addressAddressData], function(err, result){
         callback(err, result);

         });
         */
        callback(err,result);
    });

};

exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
    var queryData = [params.street, params.zipcode,params.address_id];
    console.log("PARAMS: ",params);
    console.log( queryData);
    connection.query(query, queryData, function(err, result) {
        callback(err, result);

    });
};

exports.edit = function(address_id, callback) {
    console.log("calling getinfo");
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
